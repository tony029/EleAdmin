/**
 * 这是数据定义文件
 */
/**
 * @typedef   {object}         FieldExt   --域扩展定义
 * $attrs 直接绑定到 el-field
 */

/**
 * @typedef   {object}          Element   --元素
 * @property  {string|number}     index   --索引
 * @property  {string}            label   --标签
 * @property  {string}             name   --绑定
 * @property  {?string}         element   --域标记
 * @property  {?*}         defaultValue   --默认值
 * @property  {?Array.<object>}}  rules   --校验信息
 * @property  {?Array.<Pair>}   options   --子选项
 * @property  {?object}           props   --发给el-form-item的props
 * @property  {?FieldExt}         field   --field定义
 * @property  {?Column}          column   --column定义
 * @property {?number}            width --列宽
 * @property {?boolean}        sortable --可排序
 * @property {?string}      headerAlign --表头对齐方式
 * @property {?string}            align --列数据对齐方式
 * @property  {?boolean|EditCell}   edit --表格列可编辑
 * $attrs 直接绑定到 el-column
 */

/**
 * @typedef {object} EditCell 可编辑列定义
 * @property {string} url
 */

/**
 * @typedef {object} Button --按钮定义
 * @property {string|number} index --名称
 * @property {string}         name --名称
 * @property {string}        label --标签
 * @property {?object}      dialog --对话框
 * @property {?object}    redirect --重定向
 * @property {?string}         url --api_url
 * @property {?Payload}    payload --负载处理
 */

/**
 * @typedef {object|boolean|string} Payload 负载处理指示
 *   false: null,
 *   true: id,
 *   undefined: row,
 *   [string]: {
 *     id: row.id,
 *     name: [string],
 *     value: action.value
 *   }
 */

/**
 * @typedef {object} Action --按钮列定义
 * @property {Toolbar} toolbar --按钮列表属性
 * @property {object}  column  --列属性
 */

/**
 * @typedef {object} Table --表格定义
 * @property {?boolean|object}  selector --选择列定义
 * @property {Array.<Element>} columns --列列表
 * @property {Array.<Action>}  actions --操作列
 */

/**
 * @typedef {object} Toolbar --工具栏定义
 * @property {Array.<Button>} items
 */

/**
 * @typedef {object} TableView --表格视图定义
 * @property {string}         url         --用于数据更新的API地址
 * @property {Array.<object>} rows        --数据
 * @property {Table}          table       --表格
 * @property {object}         pagination  --分页栏
 * @property {Toolbar}        toolbar     --工具栏
 * @property {Array.<number>} selected    --选择的行
 */

/**
 * @typedef {object} Pair
 * @property {string} name
 * @property {string} value
 */

/**
 * @typedef   {object} Form            --表单定义
 * @property  {Array.<Element>} fields --域定义
 * $attrs 直接绑定到 el-form
 * }}
 */
//
// export const FIELD_LIST = 'ed-field-list'
// export const FIELD_GROUP = 'ed-field-group-button'
// export const FIELD_PAIR = 'ed-pair'
// // export const FIELD_INNER = 'span'
// export const FIELD_CHECKBOX = 'el-checkbox-group'
// export const FIELD_RADIO = 'el-radio-group'
// export const FIELD_SELECT = 'el-select'
// export const FIELD_MELECT = 'el-melect'
// export const FIELD_INPUT = 'el-input'
// export const FIELD_TEXT = 'el-text'
// export const FIELD_PASSWORD = 'el-password'
// export const FIELD_NUMBER = 'el-input-number'
// export const FIELD_CASCADER = 'el-cascader'
// export const FIELD_SWITCH = 'el-switch'
// export const FIELD_SLIDER = 'el-slider'
// export const FIELD_TIME = 'el-time-picker'
// export const FIELD_DATE = 'el-date-picker'
// export const FIELD_DATETIME = 'el-date-time-picker'
// export const FIELD_UPLOAD = 'el-upload'
// export const FIELD_RATE = 'el-rate'
// export const FIELD_COLOR = 'el-color-picker'
// export const FIELD_TRANSFER = 'el-transfer'
// export const FIELD_AVATAR = 'el-avatar-upload'
// export const FIELDS = {
//   items: {
//     [FIELD_COLOR]: {
//       tag: 'el-color-picker'
//     },
//     [FIELD_UPLOAD]: {
//       tag: 'ed-file-upload',
//       default: () => ''
//     },
//     [FIELD_TRANSFER]: {
//       tag: 'el-transfer',
//       default: () => []
//     },
//     [FIELD_DATETIME]: {
//       tag: 'el-date-picker',
//       type: 'datetime'
//     },
//     [FIELD_DATE]: {
//       tag: 'el-date-picker'
//     },
//     [FIELD_SLIDER]: {
//       tag: 'el-slider'
//     },
//     [FIELD_NUMBER]: {
//       tag: 'el-input-number'
//     },
//     [FIELD_TIME]: {
//       tag: 'el-time-picker'
//     },
//     [FIELD_SWITCH]: {
//       tag: 'el-switch'
//     },
//     [FIELD_NUMBER]: {
//       tag: 'el-input-number'
//     },
//     [FIELD_RATE]: {
//       tag: 'el-rate'
//     },
//     [FIELD_CASCADER]: {
//       tag: 'el-cascader'
//     },
//     [FIELD_CHECKBOX]: {
//       tag: 'el-checkbox-group',
//       option: (attr) => ({
//         tag: 'el-checkbox',
//         label: attr.value,
//         text: attr.label
//       }),
//       default: () => []
//     },
//     [FIELD_SELECT]: {
//       tag: 'el-select',
//       option: (attr) => ({
//         tag: 'el-option',
//         ...attr,
//         value: attr.value + ''
//       })
//     },
//     [FIELD_MELECT]: {
//       tag: 'el-select',
//       option: (attr) => ({
//         tag: 'el-option',
//         ...attr
//       }),
//       props: {
//         multiple: true
//       },
//       default: () => []
//     },
//     [FIELD_RADIO]: {
//       tag: 'el-radio-group',
//       option: (option) => ({
//         tag: 'el-radio',
//         ...option,
//         label: option.value + '',
//         text: option.label
//       })
//     },
//     [FIELD_INPUT]: {
//       tag: 'el-input'
//     },
//     [FIELD_PASSWORD]: {
//       tag: 'el-input',
//       type: 'password'
//
//     },
//     [FIELD_TEXT]: {
//       tag: 'el-input',
//       type: 'textarea',
//       block: true,
//       autosize: {
//         minRows: 2,
//         maxRows: 6
//       }
//     },
//     [FIELD_PAIR]: {
//       tag: 'ed-pair',
//       block: true,
//       editable: false,
//       default: () => ({})
//     },
//     [FIELD_GROUP]: {
//       tag: 'ed-field-group-button',
//       block: true,
//       editable: false,
//       default: () => ({})
//     },
//     [FIELD_LIST]: {
//       tag: 'ed-field-list',
//       block: true,
//       editable: false,
//       default: () => []
//     },
//     [FIELD_AVATAR]: {
//       tag: 'ed-avatar-upload'
//     },
//     default: {
//       tag: 'el-input'
//     }
//   }
// }
//
// export const pick = (tag) => {
//   return FIELDS.items[tag] || FIELDS.items['default']
// }
//
// export const isBlock = (element) => {
//   return !!pick(element.tag).block
// }
// export const getDefaultValue = (element) => {
//   // 自带默认值优先 默认值必须是原始值或函数
//   let value = element.defaultValue
//
//   if (value !== undefined) return value
//
//   const d = pick(element.tag).default
//
//   if (d && {}.toString.call(d) === '[object Function]') return d()
//
//   return d
// }
//
// export const FieldFactory = {
//   'fields': [
//     {
//       label: '类型',
//       tag: FIELD_SELECT,
//       name: 'tag',
//       defaultValue: 'el-input',
//       col: 6,
//       options: [
//         { label: '单行文本', value: FIELD_INPUT },
//         { label: '多行文本', value: FIELD_TEXT },
//         { label: '密码文本', value: FIELD_PASSWORD },
//         { label: '切换开关', value: FIELD_SWITCH },
//         { label: '数值按钮', value: FIELD_NUMBER },
//         { label: '数值滑块', value: FIELD_SLIDER },
//         { label: '单项选择', value: FIELD_SELECT },
//         { label: '多项选择', value: FIELD_MELECT },
//         { label: '单选按钮', value: FIELD_RADIO },
//         { label: '复选按钮', value: FIELD_CHECKBOX },
//         { label: '级联选择', value: FIELD_CASCADER },
//         { label: '时间选择', value: FIELD_TIME },
//         { label: '日期选择', value: FIELD_DATE },
//         { label: '时间日期', value: FIELD_DATETIME },
//         { label: '文件上传', value: FIELD_UPLOAD },
//         { label: '五星评分', value: FIELD_RATE },
//         { label: '颜色选择', value: FIELD_COLOR }
//       ]
//     },
//     {
//       label: '名称',
//       tag: FIELD_INPUT,
//       name: 'name',
//       col: 6
//     },
//     {
//       label: '标签',
//       tag: FIELD_INPUT,
//       name: 'label',
//       col: 6
//     },
//     {
//       label: '默认值',
//       tag: FIELD_INPUT,
//       name: 'defaultValue',
//       col: 6
//     },
//     {
//       label: '选项',
//       tag: FIELD_LIST,
//       name: 'options',
//       fields: [
//         {
//           name: 'label',
//           label: '选项名称'
//         },
//         {
//           name: 'value',
//           label: '选项值'
//         }
//       ],
//       when: {
//         name: 'tag',
//         value: [
//           FIELD_GROUP,
//           FIELD_LIST,
//           FIELD_CHECKBOX,
//           FIELD_CASCADER,
//           FIELD_MELECT,
//           FIELD_RADIO,
//           FIELD_SELECT,
//           FIELD_TRANSFER
//         ]
//       }
//     },
//     {
//       label: '子元素',
//       tag: FIELD_PAIR,
//       name: 'fields'
//     }
//   ]
// }
