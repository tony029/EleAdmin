/**
 * 表单域名称定义
 */

export const FIELD_INPUT     = 'input'
export const FIELD_TEXT      = 'text'

export const FIELD_CHECKBOX  = 'checkbox'
export const FIELD_RADIO     = 'radio'
export const FIELD_SELECT    = 'select'
export const FIELD_CASCADER  = 'cascader'

export const FIELD_SWITCH    = 'switch'
export const FIELD_SLIDER    = 'slider'
export const FIELD_RATE      = 'rate'
export const FIELD_COLOR     = 'color'

export const FIELD_TIME      = 'time'
export const FIELD_DATE      = 'date'

export const FIELD_UPLOAD    = 'upload'
export const FIELD_AVATAR    = 'avatar'

export const FIELD_EDITOR    = 'editor'
export const FIELD_AUTO      = 'auto'

/**
 * 标签(string)
 */
export const LABEL = 'label';
/**
 * 名称(string)
 */
export const NAME = 'name';
/**
 * 默认值(string)
 */
export const DFT = 'default';
/**
 * (string)
 */
export const TYPE = 'type';
/**
 * (object) 定义
 */
export const DEFINITION = 'definition';
/**
 * 选项
 */
export const FIELD_OPTIONS   = 'options'
/**
 * 组件
 */
export const WIDGET          = 'widget'
/**
 * XHtml标签
 */
export const TAG             = 'tag'

export const OPTIONS         = 'options'

/**
 * 配置项定义文件
 */
export const Pearls = {
  [NAME]: {
    [NAME]: NAME,
    [LABEL]: '标识符',
    [WIDGET]: FIELD_AUTO,
    rules: [],
    required: true
  },
  [LABEL]: {
    [NAME]: LABEL,
    [LABEL]: '名称',
    [WIDGET]: FIELD_INPUT,
    required: true,
  },
  [TYPE]: {
    [NAME]: TYPE,
    [LABEL]: '文本框类型',
    [WIDGET]: FIELD_SELECT,
    [OPTIONS]: [
      {
        label: '文本',
        value: 'text'
      },
      {
        label: '密码',
        value: 'password'
      }
    ],
    [DFT]: 'text'
  },
  rules: {
    [NAME]: 'rules',
    [LABEL]: '校验',
    [WIDGET]: FIELD_CHECKBOX,
    [OPTIONS]: [
      {
        label: '邮件',
        value: 'email'
      },{
        label: '手机号',
        value: 'phone'
      }, {
        label: '身份证',
        value: 'ID'
      }
    ]
  },
  length: {
    [NAME]: 'length',
    [LABEL]: '长度',
    [WIDGET]: FIELD_SLIDER,
    range: true,
    [DFT]: [0, 50]
  },
  [OPTIONS]: {
    [NAME]: OPTIONS,
    [DEFINITION]: '选项',
    [WIDGET]: FIELD_TEXT
  },
  placeholder: {
    [NAME]: 'placeholder',
    [LABEL]: '占位文字',
    [WIDGET]: FIELD_INPUT
  },
  col: {
    [NAME]: 'col',
    [LABEL]: '排版',
    [WIDGET]: FIELD_SLIDER,
    max: 24,
    min: 1,
    step: 1,
    showStops: true,
    [DFT]: 24
  }
}

export const COMPONENTS = 'components'

export const Auto = {
  [WIDGET]: FIELD_AUTO,
  [COMPONENTS]: [
    Pearls.col,
    Pearls[LABEL],
    Pearls.placeholder
  ],
  [DFT]: null
}

export const Input = {
  [WIDGET]: FIELD_INPUT,
  [COMPONENTS]: [
    Pearls.col,
    Pearls[LABEL],
    Pearls.placeholder,
    Pearls[TYPE],
    Pearls.length,
    Pearls.rules,
    {
      [NAME]: 'pascal',
      [LABEL]: '数据类型',
      [WIDGET]: FIELD_SELECT,
      [OPTIONS]: [
        {
          label: '文本',
          value: 'string'
        },
        {
          label: '整数',
          value: 'int'
        },
        {
          label: '小数',
          value: 'float'
        }
      ],
      [DFT]: 'string'
    }
  ],
  [DFT]: null
}

export const Text = {
  [WIDGET]: FIELD_TEXT,
  [COMPONENTS]: [
    // 标识符

    Pearls[LABEL]
  ],
  [DFT]: null
}

export const Radio = {
  [WIDGET]: FIELD_RADIO,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    Pearls[OPTIONS]
  ],
  [DFT]: null
}

export const Checkbox = {
  [WIDGET]: FIELD_CHECKBOX,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    Pearls[OPTIONS]
  ],
  [DFT]: () => []
}

export const Select = {
  [WIDGET]: FIELD_SELECT,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    Pearls[OPTIONS],
    {
      [NAME]: 'multi',
      [LABEL]: '多选',
      [WIDGET]: FIELD_SWITCH,
      [DFT]: false
    },
    {
      [NAME]: 'filterable',
      [LABEL]: '启用搜索',
      [WIDGET]: FIELD_SWITCH,
      [DFT]: false
    },
    {
      [NAME]: 'remote',
      [LABEL]: '远程搜索地址',
      [WIDGET]: FIELD_INPUT,
      [DFT]: null
    }
  ],
  [DFT]: (field) => field.multi ? [] : null
}

export const Cascader = {
  [WIDGET]: FIELD_CASCADER,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    Pearls[OPTIONS]
  ],
  [DFT]: () => []
}

export const Switch = {
  [WIDGET]: FIELD_SWITCH,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    {
      [NAME]: 'active-icon-class',
      [LABEL]: '打开状态图标的类名',
      [WIDGET]: FIELD_INPUT,
      [DFT]: null
    },
    {
      [NAME]: 'inactive-icon-class',
      [LABEL]: '关闭状态图标的类名',
      [WIDGET]: FIELD_INPUT,
      [DFT]: null
    },
    {
      [NAME]: 'active-text',
      [LABEL]: '打开状态文字描述',
      [WIDGET]: FIELD_INPUT,
      [DFT]: null
    },
    {
      [NAME]: 'inactive-icon-class',
      [LABEL]: '关闭状态文字描述',
      [WIDGET]: FIELD_INPUT,
      [DFT]: null
    }
  ]
}

export const Slider = {
  [WIDGET]: FIELD_SLIDER,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    {
      [NAME]: 'min',
      [LABEL]: '最小值',
      [DFT]: 0,
      [WIDGET]: FIELD_INPUT,
      translate: v => parseInt(v)
    },
    {
      [NAME]: 'max',
      [LABEL]: '最大值',
      [WIDGET]: FIELD_INPUT,
      translate: v => parseInt(v),
      [DFT]: 50
    },
    {
      [NAME]: 'step',
      [LABEL]: '步长',
      [WIDGET]: FIELD_INPUT,
      translate: v => parseInt(v),
      [DFT]: 1
    },
    {
      [NAME]: 'show-stops',
      [LABEL]: '显示间断点',
      [WIDGET]: FIELD_SWITCH,
      [DFT]: false
    },
    {
      [NAME]: 'range',
      [LABEL]: '范围选择',
      [WIDGET]: FIELD_SWITCH,
      [DFT]: false
    },
  ],
  [DFT]: (field) => (field && field.range) ? [] : null
}

export const Rate = {
  [WIDGET]: FIELD_RATE,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL]
  ],
  [DFT]: null
}

export const Color = {
  [WIDGET]: FIELD_COLOR,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    {
      [NAME]: 'show-alpha',
      [LABEL]: '透明度',
      [WIDGET]: FIELD_SWITCH,
      [DFT]: false
    }
  ]
}

export const Time = {
  [WIDGET]: FIELD_TIME,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    Pearls.placeholder
  ]
}

export const Date = {
  [WIDGET]: FIELD_DATE,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    Pearls.placeholder,
    {
      [NAME]: 'type',
      [LABEL]: '类型',
      [WIDGET]: FIELD_SELECT,
      [OPTIONS]: [
        {
          label: '年',
          value: 'year'
        },
        {
          label: '月',
          value: 'month'
        },
        {
          label: '日',
          value: 'date'
        },
        {
          label: '时',
          value: 'time'
        }
      ]
    }
  ]
}

export const Upload = {
  [WIDGET]: FIELD_UPLOAD,
  [COMPONENTS]: [
    Pearls.col,
    Pearls[LABEL],
    {
      [NAME]: 'url',
      [LABEL]: '上传地址',
      [WIDGET]: FIELD_INPUT,
      required: true,
      [DFT]: null
    },
    {
      [NAME]: 'complete-label',
      [LABEL]: '完成文字',
      [WIDGET]: FIELD_INPUT,
      required: true,
      [DFT]: null
    },
    {
      [NAME]: 'preload-label',
      [LABEL]: '按钮标题',
      [WIDGET]: FIELD_INPUT,
      required: true,
      [DFT]: null
    },
    {
      [WIDGET]: FIELD_SWITCH,
      [NAME]: 'multiple',
      [LABEL]: '多选',
      [DFT]: false
    },
    {
      [WIDGET]: FIELD_INPUT,
      [NAME]: 'accept',
      [LABEL]: '类型限制',
      [DFT]: null
    },
    {
      [WIDGET]: FIELD_INPUT,
      [NAME]: 'limit',
      [LABEL]: '个数限制',
      [DFT]: null
    }
  ]
}

export const Avatar = {
  [WIDGET]: FIELD_AVATAR,
  [COMPONENTS]: [
    Pearls.col,

    Pearls[LABEL],
    {
      [NAME]: 'url',
      [LABEL]: '上传地址',
      [WIDGET]: FIELD_INPUT,
      required: true,
      [DFT]: null
    },
    {
      [NAME]: 'width',
      [LABEL]: '图片宽px',
      [WIDGET]: FIELD_INPUT,
      [DFT]: '178px'
    },
    {
      [NAME]: 'height',
      [LABEL]: '图片高px',
      [WIDGET]: FIELD_INPUT,
      [DFT]: '178px'
    }
  ]
}

export const Editor = {
  [WIDGET]: FIELD_EDITOR,
  [COMPONENTS]: [
    Pearls[NAME]
  ]
}

export const Options = {
  [WIDGET]: FIELD_OPTIONS,
  [DFT]: () => ({})
}

/**
 * 表单域列表
 */
export const commons = [
  {
    [WIDGET]: FIELD_INPUT,
    icon: 'icon-input',
    [LABEL]: '定长文本'
  },
  {
    [WIDGET]: FIELD_TEXT,
    icon: 'icon-textarea',
    [LABEL]: '变长文本'
  },
  {
    [WIDGET]: FIELD_SWITCH,
    [LABEL]: '开关',
    icon: 'icon-switch-ON'
  },
  {
    [WIDGET]: FIELD_SLIDER,
    icon: 'icon-number',
    [LABEL]: '滑块'
  },
  {
    [WIDGET]: FIELD_RADIO,
    icon: 'icon-danxuankuang',
    [LABEL]: '单项选择'
  },
  {
    [WIDGET]: FIELD_CHECKBOX,
    icon: 'icon-checkBox',
    [LABEL]: '多项选择'
  },
  {
    [WIDGET]: FIELD_TIME,
    icon: 'icon-shijian',
    [LABEL]: '时间选择'
  },
  {
    [WIDGET]: FIELD_DATE,
    icon: 'icon-258',
    [LABEL]: '日期选择'
  },
  {
    [WIDGET]: FIELD_RATE,
    icon: 'icon-rate',
    [LABEL]: '五星评价'
  },
  {
    [WIDGET]: FIELD_COLOR,
    icon: 'icon-color',
    [LABEL]: '色彩选择'
  },
  {
    [WIDGET]: FIELD_SELECT,
    icon: 'icon-xialacaidan',
    [LABEL]: '下拉菜单'
  },
  {
    [WIDGET]: FIELD_CASCADER,
    icon: 'icon-xialacaidan',
    [LABEL]: '级联菜单'
  },
  {
    [WIDGET]: FIELD_UPLOAD,
    icon: 'icon-xialacaidan',
    [LABEL]: '文件上传'
  },
  {
    [WIDGET]: FIELD_AVATAR,
    icon: 'icon-xialacaidan',
    [LABEL]: '照片上传'
  },
  {
    [WIDGET]: Editor,
    icon: 'icon-editor',
    [LABEL]: '编辑器'
  }
]
