import YmAvatarUploader from '@/ym/components/form/YmAvatarUploader'

import * as Element from  '@/store/elements'

/**
 * ElementUI 定义文件
 */

/**
 * 视图定义
 */
export const VIEW = 'view';

/**
 * 表单视图
 */
export const FORM_VIEW  = 'form';
/**
 * 图表视图
 */
export const CHART_VIEW = 'chart';
/**
 * 表格视图
 */
export const CURD_VIEW  = 'curd';
/**
 * 构建视图
 */
export const MAKER_VIEW = 'maker';

/**
 * 视图定义(object)
 */
export const DEFINITION  = Element.DEFINITION;

/**
 * 视图标题(string)
 */
export const TITLE = 'title';

/**
 * (array) 用于表格、表单、面板的元素定义
 */
export const ELEMENTS = 'elements';

/**
 * 列表(array) {name; label; width; sortable; display}
 */
export const COLUMNS = 'columns';
/**
 * 操作组(object)
 *   toolbar.items
 */
export const TOOLBAR = 'toolbar';
/**
 * 行操作对象(object)
 *   actions.items
 */
export const EMBEDDED = 'embedded';
/**
 * 查询参数(object) { where; elements }; { OPTIONS; FIELDS }
 */
export const SEARCH = 'search';

/**
 * 表格列可排序(boolean)
 */
export const SORTABLE = 'sortable';
/**
 * 可选择(object)
 */
export const SELECTABLE = 'selectable';

/**
 * 预选择(array)
 */
export const SELECTED = 'selected';
/**
 * 分页(object)
 */
export const PAGINATION = 'pagination';
/**
 * 数据 (array)
 */
export const ROWS = 'rows';


/**
 * 选项组(array) {label; value}
 */
export const OPTIONS = Element.OPTIONS;
/**
 * 行 (array|string) 表单的行数据 | 表单的行URL
 */
export const ROW = 'row';

/**
 * 按钮表(array) {label}
 */
export const ITEMS = 'items';


/**
 * (string)  /xxx/xxx
 */
export const URL = 'url';

/**
 * 表格显示定义(string)
 *
 */
export const DISPLAY = 'display';
/**
 * 表单显示定义
 */
export const WIDGET = Element.WIDGET;
/**
 * 前端显示样式名称(string)
 */
export const CELL = 'cell';

/**
 * 前台URL负载参数(bool|object)
 */
export const PAYLOAD = 'payload';

/**
 * 跳转URL {path; layer} | url
 */
export const REDIRECT = 'redirect';

/**
 * 提交(string|object)  url | { url; param }
 */
export const SUBMIT = 'submit';
/**
 * 域表(array) {name; label; value; col; disabled}
 */
export const FIELDS = 'fields';
/**
 * 值(mixed)
 */
export const VALUE = 'value';

export const ALIGN = 'align';
export const HEADER_ALIGN = 'header-align';
export const PLACEHOLDER = 'placeholder';
export const WIDTH = 'width';
export const HEIGHT = 'height';
export const LABEL_WIDTH = 'label-width';
export const COL = 'col';
export const DISKEY = 'diskey'

export const OPTION = 'fnOption'

export const INDEX = '__$INDEX$__'

export const WIDGETS = {
  [Element.FIELD_COLOR]: {
    [Element.TAG]: 'el-color-picker',
    [Element.WIDGET]: Element.Color
  },
  [Element.FIELD_AUTO]: {
    [Element.TAG]: 'el-select',
    [OPTION]: (attr) => ({
      [Element.TAG]: 'el-option',
      ...attr,
      value: attr.value + ''
    }),
    [Element.WIDGET]: Element.Auto,
    [DEFINITION]: {
      filterable: true,
      'allow-create': true,
      'default-first-option': true
    }
  },
  [Element.FIELD_UPLOAD]: {
    [Element.TAG]: 'ym-file-uploader',
    [Element.WIDGET]: Element.Upload
  },
  [Element.FIELD_DATE]: {
    [Element.TAG]: 'el-date-picker',
    [Element.WIDGET]: Element.Date
  },
  [Element.FIELD_SLIDER]: {
    [Element.TAG]: 'el-slider',
    [Element.WIDGET]: Element.Slider
  },
  [Element.FIELD_TIME]: {
    [Element.TAG]: 'el-time-picker',
    [Element.WIDGET]: Element.Time
  },
  [Element.FIELD_SWITCH]: {
    [Element.TAG]: 'el-switch',
    [Element.WIDGET]: Element.Switch
  },
  [Element.FIELD_RATE]: {
    [Element.TAG]: 'el-rate',
    [Element.WIDGET]: Element.Rate
  },
  [Element.FIELD_CASCADER]: {
    [Element.TAG]: 'el-cascader',
    [Element.WIDGET]: Element.Cascader
  },
  [Element.FIELD_CHECKBOX]: {
    [Element.TAG]: 'el-checkbox-group',
    [OPTION]: (attr) => ({
      [Element.TAG]: 'el-checkbox',
      label: attr.value,
      text: attr.label
    }),
    [Element.WIDGET]: Element.Checkbox
  },
  [Element.FIELD_SELECT]: {
    [Element.TAG]: 'el-select',
    [OPTION]: (attr) => ({
      [Element.TAG]: 'el-option',
      ...attr,
      value: attr.value + ''
    }),
    [Element.WIDGET]: Element.Select
  },
  [Element.FIELD_RADIO]: {
    [Element.TAG]: 'el-radio-group',
    [OPTION]: (option) => ({
      [Element.TAG]: 'el-radio',
      ...option,
      label: option.value + '',
      text: option.label
    }),
    [Element.WIDGET]: Element.Radio
  },
  [Element.FIELD_INPUT]: {
    [Element.TAG]: 'el-input',
    [Element.WIDGET]: Element.Input
  },
  [Element.FIELD_TEXT]: {
    [Element.TAG]: 'el-input',
    [Element.WIDGET]: Element.Text,
    [DEFINITION]: {
      type: 'textarea',
      block: true,
      autosize: {
        minRows: 2,
        maxRows: 6
      }
    }
  },
  [Element.FIELD_AVATAR]: {
    [Element.TAG]: YmAvatarUploader,
    [Element.WIDGET]: Element.Avatar
  },
  default: {
    [Element.TAG]: 'el-input',
    [Element.WIDGET]: Element.Input
  },
  pick: (widget) => {
    return WIDGETS[widget] || WIDGETS['default']
  },
  /**
   * 获取组件默认值
   * @param field 域定义
   * @returns { null|[] }
   */
  getDefaultValue: (field) => {
    const d = getValue(field)
    if (d && {}.toString.call(d) === '[object Function]') return d(field)
    return d
  }
}

function getValue (field) {
  if (field[Element.DFT]) return field[Element.DFT]

  if (!field[WIDGET]) return null

  const widget = WIDGETS.pick(field[WIDGET])
  if (widget[Element.DFT] !== undefined) return widget[Element.DFT]

  if (!widget[Element.WIDGET]) return null
  return widget[Element.WIDGET][Element.DFT]
}
