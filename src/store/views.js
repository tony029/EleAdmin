const Views = {
  main: 'YmMain',
  form: 'YmFormView',
  curd: 'YmCurd',
  menu: 'YmMenu',
  table: 'YmTable',
  chart: 'YmChart',
  maker: 'YmMaker'
}

export default Views
