export function setView (vue, value) {
  vue.$store.commit('setView', value)
  vue.$once('hook:beforeDestroy', () => {
    vue.$store.commit('dropView', value.layer)
  })
}
