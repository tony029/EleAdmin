import Vue from 'vue'
import ElementUI from 'element-ui'
import Http from './util/http'
import store from './store'
import Ym from './ym'
import 'element-ui/lib/theme-chalk/index.css'
import '@/icons/iconfont.css'
import App from './App.vue'

Vue.use(ElementUI)

Vue.use(Ym)

Vue.use(Http, {
  // baseURL: 'http://127.0.0.1/index.php',
  timeout: 3000
})

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
